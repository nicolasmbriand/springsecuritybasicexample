package mlv.ss.SpringSecurityTest.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

class AdminFilterTest {

    private AdminFilter adminFilter;

    @BeforeEach
    public void setUp() {
        SecurityContextHolder.clearContext();
        adminFilter = new AdminFilter("passwordExpected");
    }

    @Test
    void testDoAdminFilterInternal_noHeader() throws ServletException, IOException {
        var request = new MockHttpServletRequest();
        var response = new MockHttpServletResponse();
        var filterChain = Mockito.mock(FilterChain.class);

        adminFilter.doFilterInternal(request, response, filterChain);

        verify(filterChain).doFilter(request, response);

        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    void testDoAdminFilterInternal_wrongPassword() throws ServletException, IOException {
        var request = new MockHttpServletRequest();
        request.addHeader("password", "wrongPassword");
        var response = new MockHttpServletResponse();
        var filterChain = Mockito.mock(FilterChain.class);

        adminFilter.doFilterInternal(request, response, filterChain);

        assertEquals(HttpServletResponse.SC_FORBIDDEN, response.getStatus());

        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    void testDoAdminFilterInternal_correctPassword() throws ServletException, IOException {
        var request = new MockHttpServletRequest();
        request.addHeader("password", "passwordExpected");
        var response = new MockHttpServletResponse();
        var filterChain = Mockito.mock(FilterChain.class);

        adminFilter.doFilterInternal(request, response, filterChain);

        verify(filterChain).doFilter(request, response);

        assertNotNull(SecurityContextHolder.getContext().getAuthentication());
        assertTrue(SecurityContextHolder.getContext().getAuthentication() instanceof AdminAuthentication);
    }
}
