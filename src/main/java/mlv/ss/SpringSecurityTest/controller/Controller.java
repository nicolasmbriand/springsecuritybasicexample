package mlv.ss.SpringSecurityTest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping("/publicPage")
    public String publicPage() {
        return "This is public ✌️";
    }

    @GetMapping("/privatePage")
    public String privatePage() {
        return "This is private 👎";
    }

    @GetMapping("/admin")
    public String admin() {
        return "Yeah 🎉🥳";
    }

}
