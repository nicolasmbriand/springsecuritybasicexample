package mlv.ss.SpringSecurityTest.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Collections;
public class AdminFilter extends OncePerRequestFilter {

    private final String passwordExpected;

    public AdminFilter(String passwordExpected) {
        this.passwordExpected = passwordExpected;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain)
            throws ServletException, IOException {

        // Must process the request
        if (!Collections.list(request.getHeaderNames()).contains("password")) {
            filterChain.doFilter(request, response);
            return;
        }

        String password = request.getHeader("password");

        if (passwordExpected.equals(password)) {
            // Update security context
            SecurityContext context = SecurityContextHolder.createEmptyContext();
            context.setAuthentication(
                    new AdminAuthentication()
            );
            SecurityContextHolder.setContext(context);
            // Next
            filterChain.doFilter(request, response);
        } else {
            // Deny
            response.setStatus(HttpStatus.FORBIDDEN.value());
            response.getWriter().println("You don't have access");
            return;
        }
    }
}
