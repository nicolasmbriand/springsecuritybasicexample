package mlv.ss.SpringSecurityTest.config;

import mlv.ss.SpringSecurityTest.filter.AdminFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.Collections;

@Configuration
public class SecurityConfig {

    @Value("${spring.security.api.password}")
    private String passwordExpected;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return  http.authorizeHttpRequests()
                .requestMatchers("/privatePage").authenticated()
                .requestMatchers("/admin").hasRole("admin")
                    .anyRequest().permitAll()
                .and()
                    .formLogin()
                .and()
                    .addFilterBefore(new AdminFilter(passwordExpected), UsernamePasswordAuthenticationFilter.class)
                .build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new InMemoryUserDetailsManager(new User("user", "{noop}password", Collections.emptyList()));
    }
}
